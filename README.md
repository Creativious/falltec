# Fall-Tec

## Table of Contents

* [About](#about)
* [Requirements](#requirements)
* [Versioning](#versioning)


## About

A mod that creates a Fallout like experience


## Requirements

* Minecraft Forge 1.17.1 - [Link](https://files.minecraftforge.net/net/minecraftforge/forge/index_1.17.1.html)
* Java 16 - [Link](https://adoptium.net/releases.html?variant=openjdk16&jvmVariant=hotspot)
* Creativious Library - [Link](https://gitlab.com/Creativious/creativiouslibrary)


## Versioning

Copied from the comments in the build.gradle

###### Uses the [SemVer](https://semver.org/) method of versioning

```Major - Major rewrites, such as redoing all the code therefore safe to assume that it will be completely incompatible with older versions
Minor - Features releases - Safe to assume it will be compatible with older versions
Patch - Bug fixes/Optimization/
If it has '-experimental' on the end then it is not release ready and shouldn't be published on curseforge
Say 1.0.0-experimental.1
We increase the .1 by a number every time we do something, and then we make a release of let's say 1.0.0 when we're done experimenting and it can be released as an actual release
-experimental versions are only for gitlab releases and for testing```